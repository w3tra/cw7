import React from "react"
import './Item.css'

const Item = props => {
  return (
    <div className="item" onClick={props.click} id={props.name}>
      <img src={props.image} alt={props.name}/>
      <p className="item-name">{props.name}</p>
      <p className="item-price">{props.price}</p>
    </div>
  )
};

export default Item;