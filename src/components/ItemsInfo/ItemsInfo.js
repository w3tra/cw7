import drinkImg from '../../assets/images/drink.png'
import dishImg from '../../assets/images/dish.png'
const ItemsInfo = () => [
  {name: 'Hamburger', price: 80, image: dishImg},
  {name: 'Coffee', price: 70, image: drinkImg},
  {name: 'Cheeseburger', price: 90, image: dishImg},
  {name: 'Tea', price: 50, image: drinkImg},
  {name: 'Fries', price: 45, image: dishImg},
  {name: 'Cola', price: 40, image: drinkImg}
];

export default ItemsInfo;