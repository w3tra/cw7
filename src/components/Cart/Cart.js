import React from "react"
import './Cart.css'

const Cart = props => {

  let total = 0;
  props.items.forEach((item) => {
    total += item.count * item.price;
  });

  return (
    <div className="cart">
      {props.items.map((item, index) => (
        <div key={index} className="cart-item">
          <p>{item.name} {item.price} x {item.count} = {item.price * item.count}
            <button type="button" onClick={props.remove} id={"remove-" + item.name}>x</button>
          </p>
        </div>
      ))}
      Total: {total}
    </div>
  )
};

export default Cart;