import React, { Component } from 'react';
import ItemInfo from './components/ItemsInfo/ItemsInfo'
import Item from './components/Item/Item'
import Cart from './components/Cart/Cart'

import './App.css';

class App extends Component {

  state = {
    items: []
  };

  addItemToCart = event => {
    const items = [...this.state.items];
    const item = (new ItemInfo()).find(x => x.name === event.currentTarget.id);
    let cartItem = items.find(x => x.name === item.name);
    if (cartItem) {
      cartItem.count++
    } else {
      item.count = 1;
      items.push(item);
    }
    this.setState({items});
  };

  removeItemFromCart = event => {
    const items = [...this.state.items];
    console.log(event.target.id);
    const cartItemIndex = items.findIndex(x => ('remove-' + x.name) === event.target.id);
    items.splice(cartItemIndex, 1);
    this.setState({items});
  };

  render() {
    return (
      <div className="App">
        <Cart items={this.state.items} remove={(event) => this.removeItemFromCart(event)}/>
        <div className="menu">
          { (new ItemInfo()).map((item, index) => (
            <Item name={item.name} price={item.price} image={item.image} key={index}
                  click={(event) => this.addItemToCart(event)}/>
          )) }
        </div>
      </div>
    );
  }
}

export default App;
